﻿using System;

namespace Question_3
{
    class Program
    {
        static void Main(string[] args)
        {
            var keepgoing = true;

            do
            {
                Console.WriteLine("****************************************************");
                Console.WriteLine("********** ENTER NAME OF NEW PERSON BELOW **********");
                Console.WriteLine("****************************************************");

                Console.Write(">> First Name: ");
                var fname_input = Console.ReadLine();

                Console.Write(">> Last Name: ");
                var lname_input = Console.ReadLine();

                Console.WriteLine("");
                Console.WriteLine("Creating new person record... \n");

                var user = new Person(fname_input, lname_input);

                Console.WriteLine(user.GetFullName());
                
                Console.Write("\n >>  Enter year of Birth: ");
                var birth_input = Console.ReadLine();
                int birth_year = 0;
                if (!int.TryParse(birth_input, out birth_year))
                {
                    Console.WriteLine("*******************************************************");
                    Console.WriteLine("You didn't enter a number!. The program will now close!");
                    Console.WriteLine("*******************************************************");
                    Environment.Exit(0);
                }
                else 
                {
                    user.YearOfBirth = birth_year;
                }
                Console.WriteLine("****************************************************");
                Console.WriteLine(user.PersonInfo());
                Console.WriteLine("****************************************************");

                Console.Write("Do you want to enter another person? <Y/N>: \n");

                var response = Console.ReadLine();
                
                var responseLower = response?.ToUpper();

                if (responseLower == "N")
                {
                    Console.Clear();
                    keepgoing = false;
                    Console.WriteLine("Thanks for using the program!");
                    Environment.Exit(0);
                } 
            } while (keepgoing == true);

            Console.ReadLine();
        }
    }
}
